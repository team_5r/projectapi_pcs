import 'package:flutter/material.dart';
import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'ui/jadwal.dart';
import 'ui/product.dart';
import 'ui/home.dart';
import 'ui/news.dart';
import 'ui/task.dart';

void main() {
  runApp(CurvedBottomNavigationBarPage());
}

class CurvedBottomNavigationBarPage extends StatefulWidget {
  @override
  _CurvedBottomNavigationBarState createState() =>
      _CurvedBottomNavigationBarState();
}

class _CurvedBottomNavigationBarState
    extends State<CurvedBottomNavigationBarPage> {
  final navigationIcons = const [
    Icon(Icons.home),
    Icon(Icons.card_travel),
    Icon(Icons.newspaper),
    Icon(Icons.note),
    Icon(Icons.schedule)
  ];
  int _selectedIndex = 0;
  late PageController _pageController;

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
          body: PageView(
            controller: _pageController,
            onPageChanged: (index) {
              setState(() {
                _selectedIndex = index;
              });
            },
            children: [
              _selectedIndex == 0
                  ? HomePage()
                  : _selectedIndex == 1
                      ? ProductPage()
                      : _selectedIndex == 2
                          ? NewsPage()
                          : _selectedIndex == 3
                              ? TaskPage()
                              : JadwalPage()
            ],
          ),
          bottomNavigationBar: CurvedNavigationBar(
            backgroundColor: Colors.blue,
            index: _selectedIndex,
            onTap: (index) {
              setState(() {
                _selectedIndex = index;
              });
              _pageController.jumpToPage(index);
            },
            items: navigationIcons.map((icon) => icon).toList(),
          ),
        ));
  }
}
