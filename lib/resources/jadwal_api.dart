import 'dart:convert';
import "package:http/http.dart" as http;
import '../models/jadwal.dart';

Future<List<Jadwal>> fetchDailyJadwal() async {
  final response = await http.get(Uri.parse(
      'https://raw.githubusercontent.com/lakuapik/jadwalsholatorg/master/adzan/semarang/2019/12.json'));
  if (response.statusCode == 200) {
    List jsonResponse = json.decode(response.body);
    return jsonResponse.map((jadwal) => new Jadwal.fromJson(jadwal)).toList();
  } else {
    throw Exception('Failed to load Weathers');
  }
}
