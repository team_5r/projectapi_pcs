import 'dart:convert';
import 'package:http/http.dart' as http;
import '../models/news.dart';

Future<List<News>> fetchNewsData() async {
  final response = await http
      .get(Uri.parse('https://berita-indo-api.vercel.app/v1/cnbc-news/'));
  if (response.statusCode == 200) {
    final jsonData = json.decode(response.body) as Map<String, dynamic>;
    final data = jsonData['data'] as List<dynamic>;
    return data.map((news) => News.fromJson(news)).toList();
  } else {
    // Handle the error
    throw Exception('data tidak dapat diload');
  }
}
