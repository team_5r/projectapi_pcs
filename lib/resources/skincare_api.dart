import 'dart:convert';
import "package:http/http.dart" as http;
import '../models/skincare.dart';

Future<List<Skincare>> fetchDailySkincare() async {
  final response =
      await http.get(Uri.parse('https://zeinniko.github.io/json/index.json'));
  if (response.statusCode == 200) {
    List jsonResponse = json.decode(response.body);
    return jsonResponse
        .map((skincare) => new Skincare.fromJson(skincare))
        .toList();
  } else {
    throw Exception('Failed to load Weathers');
  }
}
