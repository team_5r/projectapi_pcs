class Skincare {
  final String id;
  final String nama;
  final String keterangan;
  final String harga;
  final String gambar;

  Skincare({
    required this.id,
    required this.nama,
    required this.keterangan,
    required this.harga,
    required this.gambar,
  });

  factory Skincare.fromJson(Map<String, dynamic> json) {
    return Skincare(
      id: json['id'],
      nama: json['nama'],
      keterangan: json['keterangan'],
      harga: json['harga'],
      gambar: json['gambar'],
    );
  }
}
