import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class Detail extends StatelessWidget {
  final link;
  final title;
  final contentSnippet;
  final isoDate;
  final image;

  Detail({
    this.link,
    this.title,
    this.contentSnippet,
    this.isoDate,
    this.image,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('$title'),
      ),
      body: ListView(
        children: [
          Column(
            children: <Widget>[
              image != null
                  ? Image.network(image)
                  : Container(
                      height: 250,
                      color: Colors.grey[200],
                    ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      '$title',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 10),
                    Text(
                      '$isoDate',
                      style: TextStyle(fontStyle: FontStyle.italic),
                    ),
                    SizedBox(height: 5),
                    ElevatedButton(
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  WebViewWidget(controller: link)),
                        );
                      },
                      child: Text('Selengkapnya'),
                    ),
                    Divider(),
                    Text('Sumber: $link')
                  ],
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
