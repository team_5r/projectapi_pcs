import 'package:flutter/material.dart';
import 'package:project_pcs/models/weather.dart';
import 'package:project_pcs/resources/weather_api.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Data Cuaca'),
      ),
      body: FutureBuilder<List<Weather>>(
        future: fetchDailyWeather(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (snapshot.hasError) {
            return Center(
              child: Text('Error: ${snapshot.error}'),
            );
          } else {
            List<Weather>? weatherList = snapshot.data;

            return ListView.builder(
              itemCount: weatherList?.length,
              itemBuilder: (context, index) {
                Weather weather = weatherList![index];

                String imagePath = '';
                if (weather.cuaca.toLowerCase() == 'cerah') {
                  imagePath = '../assets/a.jpg';
                } else if (weather.cuaca.toLowerCase() == 'cerah berawan') {
                  imagePath = '../assets/c.jpg';
                } else if (weather.cuaca.toLowerCase() == 'berawan') {
                  imagePath = '../assets/a.jpg';
                }

                return ListTile(
                  leading: Image.asset(
                    imagePath,
                    width: 40,
                    height: 40,
                  ),
                  title: Text(weather.jamCuaca),
                  subtitle: Text(weather.cuaca),
                );
              },
            );
          }
        },
      ),
    );
  }
}
