import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class DetailProduct extends StatelessWidget {
  final id;
  final nama;
  final keterangan;
  final harga;
  final gambar;

  DetailProduct({
    this.id,
    this.nama,
    this.keterangan,
    this.harga,
    this.gambar,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('$nama'),
      ),
      body: ListView(
        children: [
          Column(
            children: <Widget>[
              gambar != null
                  ? Image.network(gambar)
                  : Container(
                      height: 250,
                      color: Colors.grey[200],
                    ),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      '$nama',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    SizedBox(height: 10),
                    Text(
                      '$harga',
                      style: TextStyle(fontStyle: FontStyle.italic),
                    ),
                    SizedBox(height: 5),
                    ElevatedButton(
                      onPressed: () {},
                      child: Text('Selengkapnya'),
                    ),
                    Divider(),
                    Text('Sumber: $gambar')
                  ],
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
