import 'dart:convert';
import 'package:project_pcs/ui/detail.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class NewsPage extends StatefulWidget {
  @override
  _NewsState createState() => _NewsState();
}

class _NewsState extends State<NewsPage> {
  List _posts = [];
  @override
  void initState() {
    super.initState();
    _getData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Berita Terbaru')),
      body: RefreshIndicator(
        onRefresh: _handleRefresh,
        child: ListView.builder(
          itemCount: _posts.length,
          itemBuilder: (context, index) {
            return Container(
              padding: EdgeInsets.all(20.0),
              margin: EdgeInsets.only(top: 10.0),
              child: Card(
                child: ListTile(
                  title: Text('${_posts[index]['title']}',
                      maxLines: 2, overflow: TextOverflow.ellipsis),
                  subtitle: Text(
                    '${_posts[index]['contentSnippet']}',
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (c) => Detail(
                          link: _posts[index]['link'],
                          title: _posts[index]['title'],
                          contentSnippet: _posts[index]['contentSnippet'],
                          isoDate: _posts[index]['isoDate'],
                          image: _posts[index]['image']['large'],
                        ),
                      ),
                    );
                  },
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Future<void> _handleRefresh() async {
    await _getData();
  }

  Future _getData() async {
    try {
      final response = await http
          .get(Uri.parse('https://berita-indo-api.vercel.app/v1/cnn-news'));
      if (response.statusCode == 200) {
        final data = json.decode(response.body);
        setState(() {
          _posts = data['data'];
        });
      }
    } catch (e) {
      print(e);
    }
  }
}
