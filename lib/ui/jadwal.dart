import 'package:flutter/material.dart';
import 'package:project_pcs/models/jadwal.dart';
import 'package:project_pcs/resources/jadwal_api.dart';

class JadwalPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Jadwal Shalat'),
      ),
      body: FutureBuilder<List<Jadwal>>(
        future: fetchDailyJadwal(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (snapshot.hasError) {
            return Center(
              child: Text('Error: ${snapshot.error}'),
            );
          } else {
            List<Jadwal>? jadwalList = snapshot.data;
            return ListView.builder(
              itemCount: 2,
              itemBuilder: (context, index) {
                Jadwal jadwal = jadwalList![index];
                return Column(
                  children: [
                    Container(
                      child: Text(
                        jadwal.tanggal,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Cambria',
                        ),
                      ),
                    ),
                    ListTile(
                      leading: Image.asset('../assets/download.png'),
                      title: Text('Shubuh'),
                      subtitle: Text(jadwal.shubuh),
                    ),
                    ListTile(
                      leading: Image.asset('../assets/download.png'),
                      title: Text('Dzuhur'),
                      subtitle: Text(jadwal.dzuhur),
                    ),
                    ListTile(
                      leading: Image.asset('../assets/download.png'),
                      title: Text('Ashr'),
                      subtitle: Text(jadwal.ashr),
                    ),
                    ListTile(
                      leading: Image.asset('../assets/download.png'),
                      title: Text('Magrib'),
                      subtitle: Text(jadwal.magrib),
                    ),
                    ListTile(
                      leading: Image.asset('../assets/download.png'),
                      title: Text('Isya'),
                      subtitle: Text(jadwal.isya),
                    ),
                  ],
                );
              },
            );
          }
        },
      ),

          );
  }
}
