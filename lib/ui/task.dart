import 'package:flutter/material.dart';

class TaskPage extends StatelessWidget {
  const TaskPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Task'),
      ),
      body: Container(
        child: ListView(
          children: [
            ListTile(
          leading: Image.asset('../assets/images/profile.webp'),
          title: Text('Putri Aulia'),
          subtitle: Text('Editor'),
        ),
        ListTile(
          leading: Image.asset('../assets/images/logo.jpeg'),
          title: Text('Farhan'),
          subtitle: Text('Editor'),
        ),
        ListTile(
          leading: Image.asset('../assets/images/logo.jpeg'),
          title: Text('Rizki'),
          subtitle: Text('Editor'),
        )
          ]
        ),
      ),
    );
  }
}
